angular
    .module('DocsApp')
    .factory('Endpoints',
        function (Auth, Me, Catalog) {
            return {
                Auth: Auth,
                Me: Me,
                Catalog: Catalog
            }
        })
