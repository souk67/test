angular
  .module('DocsApp')
  .factory('Auth',
    function (Data, Type, store) {
      let methods = {}

      let api = '/api/auth/'
      let name = 'Auth'

      methods['Register@Post'] = {
        link: api + 'user/register',
        parameters: {
          email: {
            type: Type.STRING,
            default: 'a@b.ru',
            validator: [{isExistAndNotEmpty: {}}]
          },
          password: {
            type: Type.STRING,
            default: '123456',
            validator: [{isExistAndNotEmpty: {}}]
          },
          firstName: {
            type: Type.STRING,
            default: 'firstName',
            validator: [{isExistAndNotEmpty: {}}]
          },
          lastName: {
            type: Type.STRING,
            default: 'lastName',
            validator: [{isExistAndNotEmpty: {}}]
          },
          middleName: {
            type: Type.STRING,
            default: 'middleName',
            validator: [{isExistAndNotEmpty: {}}]
          }
        },
        callback: function (data) {
          store.set('token', data.token)
        }
      }

      methods['Login@Post'] = {
        link: api + 'user/login',
        parameters: {
          email: {
            type: Type.STRING,
            default: 'a@b.ru',
            validator: [{isExistAndNotEmpty: {}}]
          },
          password: {
            type: Type.STRING,
            default: '123456',
            // validator: [{isExistAndNotEmpty: {}}]
          }
        },
        callback: function (data) {
          store.set('token', data.token)
        }
      }

      methods['Logout@Post'] = {
        link: api + 'user/logout',
        parameters: {},
        callback: function (data) {
          store.set('token', null)
        }
      }

      methods['Forgot@Post'] = {
        link: api + 'user/forgot',
        parameters: {
          email: {
            type: 'STRING',
            default: 'a@b.ru',
            validator: [{isExistAndNotEmpty: {}}]
          }
        },
      }
      return {
        visible: true,
        include: true,
        name: name,
        methods: methods
      }
    })
