angular
    .module('DocsApp')
    .factory('Catalog',
        function (Data, Type, store) {
            let methods = {}

            let api = '/api/v1/'
            let name = 'Catalog'

            methods['Get all Book@GET'] = {
                link: api + name.toLowerCase() + '/',
                parameters: {
                    limit: {
                        type: Type.STRING,
                        default: '5'
                    },
                    offset: {
                        type: Type.STRING,
                        default: '0'
                    },
                    search: {
                        type: Type.STRING,
                        default: ''
                    },
                    order: {
                        type: Type.STRING,
                        default: 'asc'
                    },
                    orderBy: {
                        type: Type.STRING,
                        default: 'id'
                    }
                }
            }

            methods['Add Book@POST'] = {
                link: api + name.toLowerCase() + '/',
                parameters: {
                    name: {
                        type: Type.STRING,
                        default: 'book',
                        validator: [{isExistAndNotEmpty: {}}]
                    },
                    description: {
                        type: Type.STRING,
                        default: 'description',
                        validator: [{isExistAndNotEmpty: {}}]
                    },
                    yearPublication: {
                        type: Type.DATE,
                        default: '1970-01-01 00:00:00'
                    },
                    author: {
                        type: Type.STRING,
                        default: 'task',
                        validator: [{isExistAndNotEmpty: {}}]
                    }
                }
            }

            methods['Get book@GET'] = {
                link: api + name.toLowerCase() + '/:id',
                parameters: {
                    id: {
                        type: Type.STRING,
                        default: '1',
                        validator: [{isExistAndNotEmpty: {}}]
                    }
                }
            }

            methods['Edit book@PATCH'] = {
                link: api + name.toLowerCase() + '/:id',
                parameters: {
                    id: {
                        type: Type.STRING,
                        default: '1',
                        validator: [{isExistAndNotEmpty: {}}]
                    },
                    name: {
                        type: Type.STRING,
                    },
                    viewed: {
                        type: Type.BOOLEAN,
                    },
                    author: {
                        type: Type.STRING,
                        default: 'test',
                    },
                    description: {
                        type: Type.STRING,
                        default: 'test',
                    },
                    yearPublication: {
                        type: Type.DATE,
                        default: '1970-01-01 00:00:00'
                    }
                }
            }

            methods['Delete Book@DELETE'] = {
                link: api + name.toLowerCase() + '/:id',
                parameters: {
                    id: {
                        type: Type.STRING,
                        default: '1',
                        validator: [{isExistAndNotEmpty: {}}]
                    }
                }
            }
            return {
                visible: true,
                include: true,
                name: name,
                methods: methods
            }
        })
