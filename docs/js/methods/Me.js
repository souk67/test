angular
  .module('DocsApp')
  .factory('Me',
    function (Data, Type, store) {
      let methods = {}

      let api = '/api/v1/'
      let name = 'Me'

      methods['Me@GET'] = {
        link: api + 'me',
        parameters: {},
      }

      return {
        visible: true,
        include: true,
        name: name,
        methods: methods
      }
    })
