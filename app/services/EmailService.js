const nodemailer = require('nodemailer')
let config = require('../../config')

const transporter = nodemailer.createTransport({
  service: 'gmail',
  auth: {
    user: config.email.auth.USERNAME,
    pass: config.email.auth.PASSWORD
  }
})

module.exports = {
  sendReset: (email, password) => {
    const mailOptions = {
      from: config.email.FROM,
      to: email,
      subject: 'Password Reset',
      text: 'New password for authorization ' + password
    }

    transporter.sendMail(mailOptions, function (error, info) {
      if (error) {
        return console.log(error)
      }
      console.log('Message %s sent: %s', info.messageId, info.response)
    })
  }
}
