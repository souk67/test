const router = require('koa-router')()
const {returnResult, returnError} = require('../../utils').Handlers
const {Validator} = require('../../utils')


// GET /catalog -> List all objects in JSON.
// GET /catalog/:id -> Returns the object for the given ID
// POST /catalog/ -> JSON data to add new object to db.
// POST /catalog/:id-> JSON data to add new object to db.
// PATCH /catalog/:id -> JSON data to update the object data.
// DELETE /catalog/:id -> Removes the object with the specified ID.

router.get('/', function* () {
    const ctx = this
    const parameters = Object.assign({
        limit: '10',
        offset: '0',
        order: 'asc',
        orderBy: 'id'
    }, ctx.query || {})
    let searchQuery = {}
    if (parameters.search && parameters.search.length) {
        searchQuery = {
            $or: [
                {author: {$like: '%' + parameters.search + '%'}},
                {
                    yearPublication: {$like: '%' + parameters.search + '%'}
                }]
        }
    }
    yield ctx.db.Catalog.findAndCountAll({
        where: searchQuery || {},
        order: parameters.orderBy + ' ' + parameters.order,
        limit: +parameters.limit,
        offset: +parameters.offset,
        attributes: {exclude: ['updatedAt', 'deletedAt']},
    })
        .then(result => {
            return returnResult(ctx)(result)
        })
        .catch(returnError(ctx))

})

router.get('/:id', function* () {
    const ctx = this
    yield ctx.db.Catalog.findOne({
        where: {id: ctx.params.id},
        attributes: {exclude: ['updatedAt', 'deletedAt']},
    })
        .then(result => {
            return returnResult(ctx)(result)
        })
        .catch(returnError(ctx))
})

router.post('/', function* () {
    const ctx = this
    const {parameters, errors} = Validator.validate(ctx.request.body, {
        name: [{isExistAndNotEmpty: {}}],
        description: [{isExistAndNotEmpty: {}}],
        yearPublication: [{isExistAndNotEmpty: {}}],
        author: [{isExistAndNotEmpty: {}}]
    })
    if (errors && Object.keys(errors).length) {
        return returnError(ctx, 400)(errors)
    }
    yield ctx.db.Catalog.findOrCreate({
        where: {
            name: parameters.name
        },
        defaults: parameters || {},
    })
        .then(result => {
            const book = result[0]
            const created = result[1]

            if (book) {
                if (created) {
                    return ctx.db.Catalog.findOne({
                        where: {
                            name: parameters.name
                        },
                        attributes: ['id', 'name', 'description', 'author', 'yearPublication', 'viewed']
                    })
                } else {
                    return returnError(ctx, 403)({message: 'Book already exists'})
                }
            } else {
                return returnError(ctx, 400)({message: 'Wrong data fields'})
            }
        })
        .then(returnResult(ctx))
        .catch(returnError(ctx))
})

router.patch('/:id', function* () {
    const ctx = this
    const {parameters, errors} = Validator.validate(ctx.request.body, {
        name: [{isExistAndNotEmpty: {}}],
        description: [{isExistAndNotEmpty: {}}],
        yearPublication: [{isExistAndNotEmpty: {}}],
        author: [{isExistAndNotEmpty: {}}],
        viewed: [{notRequired: {}}],
    })

    if (errors && Object.keys(errors).length) {
        return returnError(ctx, 400)(errors)
    }
    yield ctx.db.Catalog.update(parameters, {where: {id: ctx.params.id}})
        .then(returnResult(ctx))
        .catch(returnError(ctx))
})

router.delete('/:id', function* () {
    const ctx = this
    yield ctx.db.Catalog.destroy({
        where: {id: ctx.params.id}
    })
        .then(returnResult(ctx))
        .catch(returnError(ctx))
})

module.exports = router
