const router = require('koa-router')()
const koajwt = require('koa-jwt')

const config = require('../../../config')

const {returnResult, returnError} = require('../../utils').Handlers


// GET /me -> return info which contains in jwt token.

router.get('/', function* () {
  const ctx = this
  const token = (ctx.request.headers.authorization || '').split(' ')[1]
  if (token && token.length) {
    const userData = yield koajwt.verify(token, config.SECRET_KEY)
    return returnResult(ctx)(userData)
  } else {
    return returnError(ctx, 400)({message: 'Wrong credentials'})
  }
})

module.exports = router
