const router = require('koa-router')()
const koajwt = require('koa-jwt')
const md5 = require('md5')

const config = require('../../../config')

const {Handlers, Validator} = require('../../utils')

const {returnResult, returnError} = Handlers

const {EmailService} = require('../../services')

router.post('/login', function* () {
    const ctx = this

    const {parameters, errors} = Validator.validate(ctx.request.body, {
        email: [{isEmail: {}}],
        password: [{isExistAndNotEmpty: {}}]
    })

    if (errors && Object.keys(errors).length) {
        return returnError(ctx, 400)(errors)
    }
    yield ctx.db.User.findOne({
        where: {
            email: parameters.email,
            password: md5(parameters.password)
        },
        attributes: ['id', 'email', 'firstName', 'lastName', 'middleName']
    }).then(user => {
        if (user) {
            const token = koajwt.sign(user.dataValues, config.SECRET_KEY)

            ctx.set('Authorization', `Bearer ${token}`)

            return returnResult(ctx, 201)({user: user.dataValues, token: token})
        } else {
            return returnError(ctx, 400)({message: 'Wrong credentials'})
        }
    })
        .catch(returnError(ctx))
})

router.post('/logout', function* () {
    const ctx = this

    ctx.set('Authorization', null)

    return returnResult(ctx, 201)('successfully')
})

router.post('/register', function* () {
    const ctx = this
    console.log(ctx.request.body)

    const {parameters, errors} = Validator.validate(ctx.request.body, {
        email: [{isEmail: {}}],
        password: [{isExistAndNotEmpty: {}}],
        firstName: [{isExistAndNotEmpty: {}}],
        lastName: [{isExistAndNotEmpty: {}}],
        middleName: [{isExistAndNotEmpty: {}}]
    })
    console.log('2', parameters)

    if (errors && Object.keys(errors).length) {
        return returnError(ctx, 400)(errors)
    }

    yield ctx.db.User.findOrCreate({
        where: {
            email: parameters.email
        },
        defaults: parameters || {},
    })
        .then(result => {
            const user = result[0]
            const created = result[1]

            if (user) {
                if (created) {
                    return ctx.db.User.findOne({
                        where: {
                            email: parameters.email
                        },
                        attributes: ['id', 'email', 'firstName', 'lastName', 'middleName']
                    })
                } else {
                    return returnError(ctx, 403)({message: 'User already exists'})
                }
            } else {
                return returnError(ctx, 400)({message: 'Wrong data fields'})
            }
        })
        .then(user => {
            const token = koajwt.sign(user.dataValues, config.SECRET_KEY)

            ctx.set('Authorization', `Bearer ${token}`)

            return returnResult(ctx, 201)({user: user.dataValues, token: token})
        })
        .catch(returnError(ctx))
})

router.post('/forgot', function* () {
    const ctx = this

    const data = Validator.validate(ctx.request.body, {
        email: [{isEmail: {}}]
    })

    if (data.errors && Object.keys(data.errors).length) {
        return returnError(ctx, 400)(data.errors)
    }

    const parameters = data.parameters

    yield ctx.db.User.findOne({
        where: {
            email: parameters.email
        }
    })
        .then(user => {
            if (user) {
                const password = Math.round(10000 - 0.5 + Math.random() * (99999 - 10000 + 1))
                ctx.db.User.update({password: password.toString()}, { where: { email: parameters.email } })
                EmailService.sendReset(parameters.email, password)
                return returnResult(ctx, 201)({
                    message: 'Sent!'
                })
            } else {
                return returnError(ctx, 400)({message: 'Email is wrong. Please, try again'})
            }
        })
        .then(returnResult(ctx, 201))
        .catch(returnError(ctx))

    return returnResult(ctx, 201)()
})

module.exports = router
