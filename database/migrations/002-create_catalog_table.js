module.exports = {
    up: (queryInterface, Sequelize) => {
        const query = queryInterface.createTable(
            'Catalog', {
                id: {
                    type: Sequelize.INTEGER(11),
                    allowNull: false,
                    primaryKey: true,
                    autoIncrement: true
                },
                name: {
                    type: Sequelize.TEXT,
                    allowNull: false
                },
                description: {
                    type: Sequelize.TEXT,
                    allowNull: false
                },
                author: {
                    type: Sequelize.TEXT,
                    allowNull: false
                },
                yearPublication: {
                    type: Sequelize.DATE,
                    allowNull: false
                },
                viewed: {
                    type: Sequelize.BOOLEAN,
                    allowNull: false,
                    defaultValue: false
                },
                createdAt: {
                    type: Sequelize.DATE,
                    allowNull: false,
                    defaultValue: Sequelize.literal('CURRENT_TIMESTAMP')
                },
                updatedAt: {
                    type: Sequelize.DATE,
                    allowNull: false,
                    defaultValue: Sequelize.literal('CURRENT_TIMESTAMP')
                },
                deletedAt: {
                    type: Sequelize.DATE,
                    allowNull: true
                }
            })
        return query
    },

    down: (queryInterface, Sequelize) => {
        const query = queryInterface.dropTable('Catalog')

        return query
    }
}

