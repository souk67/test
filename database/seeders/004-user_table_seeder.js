const config = require('../../config')

module.exports = {
  up: function (queryInterface, Sequelize) {
    if (!config.SEED) return

    const data = [
      {id: 1, email: 'admin@admin.ru', password: 'e10adc3949ba59abbe56e057f20f883e', firstName: 'admin', lastName: 'admin', middleName: 'admin'}
    ]

    if (!data || !data.length) return

    return queryInterface.bulkInsert('User', data, {})
  },

  down: function (queryInterface, Sequelize) {
    if (!config.SEED) return

    return queryInterface.bulkDelete('User', null, {})
  }
}
