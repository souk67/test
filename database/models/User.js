const md5 = require('md5')

module.exports = function (sequelize, DataTypes) {
    return sequelize.define('User', {
        id: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        firstName: {
            type: DataTypes.CHAR(38),
            allowNull: false
        },
        lastName: {
            type: DataTypes.CHAR(38),
            allowNull: true
        },
        middleName: {
            type: DataTypes.CHAR(38),
            allowNull: true
        },
        email: {
            type: DataTypes.CHAR(255),
            allowNull: false
        },
        password: {
            type: DataTypes.TEXT,
            allowNull: false,
            defaultValue: function () {
                return md5('123456')
            },
            set: function (value) {
                this.setDataValue('password', md5(value))
            }
        },
    }, {
        tableName: 'User',
        paranoid: true
    })
}
