const md5 = require('md5')

module.exports = function (sequelize, DataTypes) {
    return sequelize.define('Catalog', {
        id: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        name: {
            type: DataTypes.TEXT,
            allowNull: false
        },
        description: {
            type: DataTypes.TEXT,
            allowNull: false
        },
        author: {
            type: DataTypes.TEXT,
            allowNull: false
        },
        yearPublication: {
            type: DataTypes.DATE,
            allowNull: false
        },
        viewed: {
            type: DataTypes.BOOLEAN,
            allowNull: false,
            defaultValue: false
        },
    }, {
        tableName: 'Catalog',
        paranoid: true
    })
}
